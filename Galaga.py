from tkinter import *
from PIL import ImageTk, Image
from threading import Thread
from random import randint

root = Tk()
root.title("Galaga")
root.geometry('500x700')

canvas1 = Canvas(root, width = 500, height = 700)
canvas1.configure(bg = "black")
canvas1.pack()

us_x = 250

def game_window():
    extra = Toplevel(root)
    extra.title("Galaga")
    extra.geometry('500x700')
    canvas2 = Canvas(extra, width = 500, height = 700)
    canvas2.configure(bg = "black")
    canvas2.pack()
    
    one_up_1 = canvas2.create_image(17, 680, image = User_Ship_Sprite)
    one_up_2 = canvas2.create_image(50, 680, image = User_Ship_Sprite)
    one_up = canvas2.create_image(60, 10, image = One_Up_Text)
    high_score = canvas2.create_image(250, 10, image = High_Score_Text)
    user_ship = canvas2.create_image(250, 648, image = User_Ship_Sprite)

    def action(event):
        global us_x
        if event.char == "a":
            if us_x > 20:
                us_x = us_x - 8
                canvas2.move(user_ship, -8, 0)
        if event.char == "d":
            if us_x < 480:
                us_x = us_x + 8
                canvas2.move(user_ship, 8, 0)

    extra.bind("<Key>", action)
    
    stars_one = []
    stars_two = []
    
    for i in range(50):
        rx = randint(0, 450)
        ry = randint(0, 650)
        stars_one.append(canvas2.create_oval(20 + rx, 20 + ry, 22 + rx, 22 + ry, fill = "#FFFFFF", outline = ""))
    for i in range(50):
        rx = randint(0, 450)
        ry = randint(0, 650)
        stars_two.append(canvas2.create_oval(20 + rx, 20 + ry, 22 + rx, 22 + ry, fill = "#888888", outline = ""))
    
    while True:
        for i in range(len(stars_one)):
            canvas2.itemconfig(stars_one[i], fill = "#888888")
            canvas2.tag_lower(stars_one[i])
        canvas2.update()
        canvas2.after(130)
        for i in range(len(stars_one)):
            canvas2.itemconfig(stars_two[i], fill = "#FFFFFF")
            canvas2.tag_lower(stars_two[i])
        canvas2.update()
        canvas2.after(130)
        for i in range(len(stars_one)):
            canvas2.itemconfig(stars_one[i], fill = "#222222")
            canvas2.tag_lower(stars_one[i])
        canvas2.update()
        canvas2.after(130)
        for i in range(len(stars_one)):
            canvas2.itemconfig(stars_two[i], fill = "#888888")
            canvas2.tag_lower(stars_two[i])
        canvas2.update()
        canvas2.after(130)
        for i in range(len(stars_one)):
            canvas2.itemconfig(stars_one[i], fill = "#888888")
            canvas2.tag_lower(stars_one[i])
        canvas2.update()
        canvas2.after(130)
        for i in range(len(stars_one)):
            canvas2.itemconfig(stars_two[i], fill = "#222222")
            canvas2.tag_lower(stars_two[i])
        canvas2.update()
        canvas2.after(130)
        for i in range(len(stars_one)):
            canvas2.itemconfig(stars_one[i], fill = "#FFFFFF")
            canvas2.tag_lower(stars_one[i])
        canvas2.update()
        canvas2.after(130)
        
game_thread = Thread(target = game_window)

image_USS = Image.open('USS.png')
image_USS = image_USS.resize((30, 30), resample = 0)
User_Ship_Sprite = ImageTk.PhotoImage(image_USS)

image_1UP = Image.open('1UP.png')
image_1UP = image_1UP.resize((48, 18), resample = 0)
One_Up_Text = ImageTk.PhotoImage(image_1UP)

image_HighScore = Image.open('High_Score.png')
image_HighScore = image_HighScore.resize((162, 18), resample = 0)
High_Score_Text = ImageTk.PhotoImage(image_HighScore)

image_Play = Image.open('Play.png')
image_Play = image_Play.resize((64, 18), resample = 0)
Play_Text = ImageTk.PhotoImage(image_Play)

play_button = Button(canvas1, image = Play_Text, border = 0, bg = "black", command = game_window)
play_button_window = canvas1.create_window(250, 341, window = play_button)

stars_one = []
stars_two = []

def menu():
    for i in range(50):
        rx = randint(0, 450)
        ry = randint(0, 650)
        stars_one.append(canvas1.create_oval(20 + rx, 20 + ry, 22 + rx, 22 + ry, fill = "#FFFFFF", outline = ""))
    for i in range(50):
        rx = randint(0, 450)
        ry = randint(0, 650)
        stars_two.append(canvas1.create_oval(20 + rx, 20 + ry, 22 + rx, 22 + ry, fill = "#888888", outline = ""))
    while True:
        for i in range(len(stars_one)):
            canvas1.itemconfig(stars_one[i], fill = "#888888")
            canvas1.tag_lower(stars_one[i])
        canvas1.update()
        canvas1.after(130)
        for i in range(len(stars_one)):
            canvas1.itemconfig(stars_two[i], fill = "#FFFFFF")
            canvas1.tag_lower(stars_two[i])
        canvas1.update()
        canvas1.after(130)
        for i in range(len(stars_one)):
            canvas1.itemconfig(stars_one[i], fill = "#222222")
            canvas1.tag_lower(stars_one[i])
        canvas1.update()
        canvas1.after(130)
        for i in range(len(stars_one)):
            canvas1.itemconfig(stars_two[i], fill = "#888888")
            canvas1.tag_lower(stars_two[i])
        canvas1.update()
        canvas1.after(130)
        for i in range(len(stars_one)):
            canvas1.itemconfig(stars_one[i], fill = "#888888")
            canvas1.tag_lower(stars_one[i])
        canvas1.update()
        canvas1.after(130)
        for i in range(len(stars_one)):
            canvas1.itemconfig(stars_two[i], fill = "#222222")
            canvas1.tag_lower(stars_two[i])
        canvas1.update()
        canvas1.after(130)
        for i in range(len(stars_one)):
            canvas1.itemconfig(stars_one[i], fill = "#FFFFFF")
            canvas1.tag_lower(stars_one[i])
        canvas1.update()
        canvas1.after(130)
    
stars_canvas = Thread(target = menu)
stars_canvas.run()

